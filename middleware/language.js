/*
 * 1. sets i18n.locale and state.locale if possible
 * 2. redirects if not with locale
 */
export default function ({ 
    isHMR, app, store, route, params, error, redirect, http, VueRouter
}) {
    if(store.state.defaultLocale && store.state.locale && store.state.locales.length){
        const { lang } = route.params;
        let locale = '';

        if(store.state.locales.indexOf(lang) == -1){
            locale = store.state.defaultLocale;
        }else{
            locale = lang;
        }

        store.commit('setLang', locale); // set store
        app.i18n.locale = store.state.locale;
    }else{
        store.dispatch('getLanguages', { app, store })
    }
}