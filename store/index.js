import HttpConfig from '~/plugins/http/http.config'

const http = HttpConfig.get();
export const state = () => ({
    locales: [],
    locale: '',
    localeDisplays: [],
    defaultLocale: '',
    template: '',
    isActive: '',
    subIsActive: '',
    loginRedirect: '',
})

export const mutations = {
    setLang(state, locale) {
        state.locale = locale;
    },
    setLocales(state, locales){
        state.locales = locales;
    },
    setLocaleDisplays(state, localeDisplays){
        state.localeDisplays = localeDisplays;
    },
    setDefaultLocale(state, defaultLocale){
        state.defaultLocale = defaultLocale;
    },
    setTemplate(state, template){
        state.template = template;
    },
    setActiveSidebar(state, data){
        let { isActive, subIsActive } = data;
        state.isActive = isActive;
        state.subIsActive = subIsActive;
    },
    setLoginRedicret(state, data){
        state.loginRedirect = data;
    }
}

export const actions = {
    getLanguages({ commit }, context){
        http.get('/languages?type=switcher')
        .then(response => {
            // Init
            const { app, store, route } = context;

            // Process
            let datas = response.data.data;
            let locales = [];
            let defaultLocale = '';
            let localeDisplays = {};
            if(datas){                
                for(let i in datas){
                    const data = datas[i];
                    if(data.is_default == 1){
                        defaultLocale  = data.code;
                    }
                    locales.push(data.code);
                    localeDisplays[data.code] = {
                        name: data.name,
                        image: data.image,
                    };
                }
            }else{
                defaultLocale = 'en';
                locales = ['en'];
            }
            const { lang } = route.params;
            const { fullPath } = route;
            let locale = defaultLocale;
            let url = '';

            if(locales.indexOf(lang) != -1){
                locale = lang;
            }else{
                url = `/${locale}`;    
            }

            commit('setLang', locale);
            commit('setLocales', locales);
            commit('setDefaultLocale', defaultLocale);
            commit('setLocaleDisplays', localeDisplays);
            app.i18n.locale = locale;

            store.dispatch('user/authentication', { url, route, store, localeDisplays });            
        })
    },
}