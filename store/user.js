import AuthService from '~/services/auth.service';

export const state = () => ({
    data: {},
})

export const mutations = {
    set (state, user) {
        state.data = user;
    },
}

export const actions = {
    authentication({ commit }, context){
        const { url, route, store, localeDisplays } = context;
        const { lang } = route.params;
        let { fullPath } = route;

        const current_template = fullPath.split('/')[2] ? fullPath.split('/')[2] : '';
        const pages_check = [
            'dashboard',
        ];
        if(url){
            this.$router.push(url);
        }
        if(pages_check.indexOf(current_template) !== -1 && !AuthService.isAuth){
            if(current_template == 'dashboard'){
                this.$router.push(`/${lang}/login/dashboard`);
            }else{
                this.$router.push(`/${lang}/login`);
            }
        }
        if(AuthService.isAuth){
            if(current_template == 'login'){
                this.$router.go(-1);
            }
            if(current_template == 'dashboard' && store.state.user.data.account_type !== 'ADMIN'){
                this.$router.go(-1);
            }           
        } 
    }
}