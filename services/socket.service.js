import io from 'socket.io-client';

const { SOCKET_PORT } = process.env;
const socket = io.connect(`${window.location.protocol}//${window.location.hostname}:${SOCKET_PORT}`);

class SocketService {

    /**
    * @description Emit event to Socket
    * @return emit event
    */
    static emit(key, data){
        socket.emit(key, data);
    }

    /**
    * @description Socket object
    * @return {void}
    */
    static get socket(){
        return socket;
    }
}

export default SocketService;
