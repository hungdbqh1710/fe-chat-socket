/**
 * @class AuthService
 * @static
 * @hideconstructor
 */
class AuthService {
    /**
    * @static
    * @description Gets access token from the localStorage
    * @return {string}
    */
    static get accessToken() {
        let token = localStorage.getItem('findGoAccessToken');
        if(token){
            return `Bearer ${token}`
        }
        return localStorage.getItem('findGoAccessToken');
    }

    /**
    * @static
    * @description Sets access token at the localStorage
    * @param {string} token
    * @return {void}
    */
    static setAccessToken(accessToken) {
        localStorage.setItem('findGoAccessToken', accessToken);
    }

    /**
    * @static
    * @return {boolean}
    */
    static get isAuth() {
        return Boolean(this.accessToken);
    }

    /**
    * @static
    * @description Clear all token of the localStorage.
    * @returns {void}
    */
    static logOut() {
        localStorage.removeItem('findGoAccessToken');
    }

    /**
    * @static
    * @description Set user login
    * @returns {void}
    */
    static setUser() {
        let token = localStorage.getItem('findGoAccessToken');
        if(token){
            const base64Url = token.split('.')[1];
            const base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse(window.atob(base64));
        }
        return {};
    }

    /**
    * Decode base64String and fix utf8 problem
    */
    static decodeJWT(token) {
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace('-', '+').replace('_', '/');
        return decodeURIComponent(atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }
}

export default AuthService;
