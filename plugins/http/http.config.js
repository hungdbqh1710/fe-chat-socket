import axios from 'axios';
import AuthService from '~/services/auth.service';

const { API_PORT } = process.env;
const apiUrl = `${window.location.protocol}//${window.location.hostname}:${API_PORT}/api/v1/`;
const lang = window.location.pathname.split('/')[1];

/**
 * Create Axios
 */
export const http = axios.create({
    baseURL: apiUrl,
})

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our NodeJS back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */
http.interceptors.request.use(
    config => {
        config.headers.common['Authorization'] = AuthService.accessToken;
        return config;
    },
    error => {
        return Promise.reject(error);
    },
);

/**
 * Handle all error messages.
 */
http.interceptors.response.use((response) => {
    // Do something with response data
    let res = {
        data: response.data,
        status: response.status
    }
    return res;
  }, (error) => {
    const { response } = error;
    let dt = response.data;
    if(response.status == 422){
        dt = dt.data;
    }
    const res = {
        status: response.status,
        data: dt
    }
    // Do something with response error
    return Promise.reject(res);
});
class HttpConfig{
	/**
	* Return http config
	*/
	static get(){
		return http;
	}
}

export default HttpConfig;