import Vue from 'vue';
import httpPlugin from './http';
import VueI18n from 'vue-i18n';
import VueToastr from '@deveodk/vue-toastr';
import VueHead from 'vue-head';
 
Vue.use(VueHead)
Vue.use(VueI18n);
Vue.use(httpPlugin);
Vue.use(VueToastr,{
    defaultPosition: 'toast-top-right',
    defaultType: 'success',
    defaultTimeout: 3000
});

export default ({ app, store }) => {
    const lang = window.location.pathname.split('/')[1];
    // Set i18n instance on app
    // This way we can use it in middleware and pages asyncData/fetch
    app.i18n = new VueI18n({
        locale: lang,
        fallbackLocale: lang,
        messages: {
            'vi': require('~/lang/vi.json'),
            'en': require('~/lang/en.json')
        }
    });
    app.i18n.path = (link) => {
        if (app.i18n.locale === app.i18n.fallbackLocale) {
            return `/${link}`;
        }
        return `/${app.i18n.locale}/${link}`;
    }
};
/**
* Every function here must have global prefix
*/

import AuthService from '../services/auth.service';

Vue.mixin({
    methods: { 
        globalImageUrl: function(path) {
            if(path){
                return `${window.location.origin}/${path}`;   
            }
            return '';
        },
        globalObjectLength: function(obj){
            let length = 0;
            if(typeof obj === 'object' && obj !== null){
                for(var i in obj){
                    length++;
                }
            }
            return length;
        },
        globalBaseUrl(path){
            return  `/${this.$store.state.locale}/${path}`;
        },
        globalAuthDirect: function(name, action = '', query = {}, params = {}){
            if(action && action == 'login'){
                this.$store.commit('setLoginRedicret', { name, query, params });
                this.$router.push('/');
            }else{
                if( !AuthService.isAuth ){
                    alert(this.$t('login_warning'));
                    this.$router.push('/');
                    this.$store.commit('setLoginRedicret', { name, query, params });
                }else{
                    this.$router.push({ name, query, params });
                }
            }
        },
        globalCheckAuth: function() {
            return AuthService.isAuth;
        },
        globalFormatNumber: function(number, seperator) {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, seperator);
        },
        globalConvertTimestamp(timestamp, format){
            return moment.unix(timestamp).format(format);
        },
    }
});
Vue.use(httpPlugin);
