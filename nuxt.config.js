const pkg = require('./package')


module.exports = {
    mode: 'spa',

    /*
    ** Headers of the page
    */
    head: {
        title: 'Training 2019',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'keywords', name: 'keywords', content: 'find, go, tour' },
            { hid: 'description', description: 'description', content: pkg.description },
            { hid: 'og:description', name: 'og:description', content: pkg.description },
            { hid: 'og:title', name: 'og:title', content: 'Find n Go' },
            { hid: 'og:image', name: 'og:image', content: '/favicon.ico' },
            { hid: 'og:site_name', name: 'og:site_name', content: 'Find n Go' },
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons' },
            { rel: "stylesheet", href: "https://use.fontawesome.com/releases/v5.8.1/css/all.css" },
            {
              rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
            },
            {
                rel: "stylesheet", href: "https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css"
            }
        ]
    },

    /*
    ** Customize the progress-bar color
    */
    loading: { color: '#fff' },

    /*
    ** Global CSS
    */
    css: [
        //'~/assets/build/css/main.css',
    ],

    /*
    ** Plugins to load before mounting the App
    */
    plugins: [
        '~/plugins/app.js',
    ],

    /*
    ** Nuxt.js modules
    */
    modules: [

    ],

    env: {
      API_PORT: 3011,
      SOCKET_PORT: 5002,
      CRON_SOCKET_PORT: 5001,
      CRAWL_SOCKET_PORT: 5004,
      SITE_NAME: 'Clevebet',
      SALT_OBJECT: '123qwe!@#-gl',
      SALT_DECODE: 'clevebet'
  },

    /*
    ** Build configuration
    */
    build: {
        /*
        ** You can extend webpack config here
        */
        extend(config, ctx) {
            vendor: ['vue-i18n']
        }
    },

    /*
    ** Router middleware
    */
    router: {
      //  middleware: 'language'
    }

}
